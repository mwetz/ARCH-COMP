function h = plotAllDim(varargin)
% plotAllDim - Plots all 2-dimensional projections of a reachable set
%    note: only for one branch (non-hybrid, non-splitting) until now
%
% Syntax:  
%    h = plotAllDim(R)
%    h = plotAllDim(R,dims)
%    h = plotAllDim(R,dims,'Color','red',...)
%
% Inputs:
%    R - reachSet object
%    dims - projected dimensions to be plotted (1xn int-vector or 'all')
%    varargin - standard plotting preferences
%
% Outputs:
%    h - handle of figure
%
% Example: 
%    -
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: polygon

% Author:       Mark Wetzlinger
% Written:      23-April-2020
% Last update:  ---
% Last revision:05-June-2020

%------------- BEGIN CODE --------------

% input processing
R = varargin{1};
type{1} = 'b';
% If only one argument is passed
if nargin==1
    dims = 'all'; % init empty to skip check
% If two or more arguments are passed
elseif nargin>=2
    dims = varargin{2};
    % If specifications also passed
    if nargin>=3
        type(1:length(varargin)-2) = varargin(3:end);
    end
end

% check which dimensions should be plotted
Zdim = dim(R.timeInterval.set{1});
if strcmp(dims,'all')
    % plot all dimensions in order up to 16 (default)
    if mod(Zdim,2) == 0
        % even dimension, no repetitions
        dims = 1:Zdim;
    else
        % odd dimension, repeat second-to-last dim for last plot
        dims = [1:Zdim-1,Zdim-1,Zdim];
    end
else
    % assert that given dims are valid
    if any(dims > Zdim) ...              % dims exceed Z dimensions
            || any(dims < 1) ...         % dims smaller than 1
            || any(mod(dims,1) ~= 0) ... % dims not natural numbers
            || mod(length(dims),2) ~= 0  % number of dims are odd
        error("Given dimensions are not valid.");
    end
end

maxPerFigure = 16;
if ceil(length(dims)) > maxPerFigure
    warning("Only the first " + maxPerFigure + " dimensions will be plotted.");
end

totalPlots = length(dims)/2;
% partition of dims into subplots
switch totalPlots
    % 1 or > 16 already excluded
    case 1
        rows = 1; cols = 1;
    case 2
        rows = 1; cols = 2;
    case 3
        rows = 1; cols = 3;
    case 4
        rows = 2; cols = 2;
    case {5,6}
        rows = 2; cols = 3;
    case {7,8}
        rows = 2; cols = 4;
end

% plotting
for sp=1:totalPlots
    subplot(rows,cols,sp); hold on; box on;

    xDim = dims(2*(sp-1)+1);
    yDim = dims(2*sp);
    
    % loop over all reachable sets
    for i=1:length(R.timeInterval.set)
    
        % project zonotope
        Zproj = project(R.timeInterval.set{i},[xDim,yDim]);

        % convert zonotope to polygon
        p = polygon(Zproj);

        % plot set
        plot(p(1,:),p(2,:),type{:});
        
    end
    
    % write axis labels
    xlabel("x_" + xDim);
    ylabel("x_" + yDim);
    
end

h = get(groot,'CurrentFigure');


end
