'''
Created by Hyst v1.6
Hybrid Automaton in Hylaa2
Converted from file: 
Command Line arguments: -gen drivetrain "-theta 2 -init_scale 0.3 -reverse_errors -switch_time 0.20001" -passes sub_constants "" simplify -p -o hylaa_drivetrain_theta2_initscale0_3.py -tool hylaa "-settings settings.aggstrat=aggstrat.Unaggregated()"
'''

import time
import sys
import numpy as np

from hylaa.hybrid_automaton import HybridAutomaton
from hylaa.settings import HylaaSettings, PlotSettings
from hylaa.core import Core
from hylaa.stateset import StateSet
from hylaa import lputil, aggstrat

def define_ha():
    '''make the hybrid automaton and return it'''

    ha = HybridAutomaton()

    # dynamics variable order: [x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, t, affine]

    negAngle = ha.new_mode('negAngle')
    a_matrix = [ \
        [0, 0, 0, 0, 0, 0, 0.0833333333333333, 0, -1, 0, 0, 0, 0], \
        [13828.8888888889, -26.6666666666667, 60, 60, 0, 0, -5, -60, 0, 0, 0, 0, 716.666666666667], \
        [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5], \
        [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, -714.285714285714, -0.04, 0, 0, 0, 714.285714285714, 0, 0, 0], \
        [-2777.77777777778, 3.33333333333333, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -83.3333333333333], \
        [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0], \
        [100, 0, 0, 0, 0, 0, 0, -1000, -0.01, 1000, 0, 0, 3], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0], \
        [0, 0, 0, 0, 1000, 0, 0, 1000, 0, -2000, -0.01, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        ]
    negAngle.set_dynamics(a_matrix)
    # x1 <= -0.03
    negAngle.set_invariant([[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], ], [-0.03, ])

    deadzone = ha.new_mode('deadzone')
    a_matrix = [ \
        [0, 0, 0, 0, 0, 0, 0.0833333333333333, 0, -1, 0, 0, 0, 0], \
        [-60, -26.6666666666667, 60, 60, 0, 0, -5, -60, 0, 0, 0, 0, 300], \
        [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5], \
        [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, -714.285714285714, -0.04, 0, 0, 0, 714.285714285714, 0, 0, 0], \
        [0, 3.33333333333333, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, -1000, -0.01, 1000, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0], \
        [0, 0, 0, 0, 1000, 0, 0, 1000, 0, -2000, -0.01, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        ]
    deadzone.set_dynamics(a_matrix)
    # -0.03 <= x1 & x1 <= 0.03
    deadzone.set_invariant([[-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], ], [0.03, 0.03, ])

    posAngle = ha.new_mode('posAngle')
    a_matrix = [ \
        [0, 0, 0, 0, 0, 0, 0.0833333333333333, 0, -1, 0, 0, 0, 0], \
        [13828.8888888889, -26.6666666666667, 60, 60, 0, 0, -5, -60, 0, 0, 0, 0, -116.666666666667], \
        [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5], \
        [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, -714.285714285714, -0.04, 0, 0, 0, 714.285714285714, 0, 0, 0], \
        [-2777.77777777778, 3.33333333333333, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 83.3333333333333], \
        [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0], \
        [100, 0, 0, 0, 0, 0, 0, -1000, -0.01, 1000, 0, 0, -3], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0], \
        [0, 0, 0, 0, 1000, 0, 0, 1000, 0, -2000, -0.01, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        ]
    posAngle.set_dynamics(a_matrix)
    # 0.03 <= x1
    posAngle.set_invariant([[-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], ], [-0.03, ])

    negAngleInit = ha.new_mode('negAngleInit')
    a_matrix = [ \
        [0, 0, 0, 0, 0, 0, 0.0833333333333333, 0, -1, 0, 0, 0, 0], \
        [13828.8888888889, -26.6666666666667, 60, 60, 0, 0, -5, -60, 0, 0, 0, 0, 116.666666666667], \
        [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -5], \
        [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, -714.285714285714, -0.04, 0, 0, 0, 714.285714285714, 0, 0, 0], \
        [-2777.77777777778, 3.33333333333333, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -83.3333333333333], \
        [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0], \
        [100, 0, 0, 0, 0, 0, 0, -1000, -0.01, 1000, 0, 0, 3], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0], \
        [0, 0, 0, 0, 1000, 0, 0, 1000, 0, -2000, -0.01, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        ]
    negAngleInit.set_dynamics(a_matrix)
    # t <= 0.20001
    negAngleInit.set_invariant([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0], ], [0.20001, ])

    error = ha.new_mode('error')
    a_matrix = [ \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        ]
    error.set_dynamics(a_matrix)

    _error = ha.new_mode('_error')

    trans = ha.new_transition(negAngleInit, negAngle)
    # t >= 0.20001
    trans.set_guard([[-0, -0, -0, -0, -0, -0, -0, -0, -0, -0, -0, -1, -0], ], [-0.20001, ])

    trans = ha.new_transition(negAngle, deadzone)
    # x1 >= -0.03
    trans.set_guard([[-1, -0, -0, -0, -0, -0, -0, -0, -0, -0, -0, -0, -0], ], [0.03, ])

    trans = ha.new_transition(deadzone, posAngle)
    # x1 >= 0.03
    trans.set_guard([[-1, -0, -0, -0, -0, -0, -0, -0, -0, -0, -0, -0, -0], ], [-0.03, ])

    trans = ha.new_transition(deadzone, error)
    # x1 <= -0.03
    trans.set_guard([[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], ], [-0.03, ])

    trans = ha.new_transition(posAngle, error)
    # x1 <= 0.03
    trans.set_guard([[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], ], [0.03, ])

    trans = ha.new_transition(error, _error)
    trans.set_guard_true()

    return ha

def define_init_states(ha):
    '''returns a list of StateSet objects'''
    # Variable ordering: [x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, t, affine]
    rv = []
    
    # 1.0 * x1 + 0.04488 = 0.0005600000000000003 * x4 - 0.01512000000000001 & 1.0 * x2 + 12.401 = 0.4669999999999999 * x4 - 12.609 & x3 = 0.0 & x5 = 0.0 & 1.0 * x6 - 27.0 = 1.0 * x4 - 27.0 & 1.0 * x7 - 324.0 = 12.0 * x4 - 324.0 & 1.0 * x8 + 0.0015 = 0.000060000000000000015 * x4 - 0.0016200000000000003 & 1.0 * x9 - 27.0 = 1.0 * x4 - 27.0 & 1.0 * x10 + 0.0015 = 0.000060000000000000015 * x4 - 0.0016200000000000003 & 1.0 * x11 - 27.0 = 1.0 * x4 - 27.0 & 27.0 <= x4 & x4 <= 33.0 & t = 0.0 & affine = 1.0
    mode = ha.modes['negAngleInit']
    mat = [[1, 0, 0, -0.0005600000000000003, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [-1, -0, -0, 0.0005600000000000003, -0, -0, -0, -0, -0, -0, -0, -0, -0], \
        [0, 1, 0, -0.4669999999999999, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [-0, -1, -0, 0.4669999999999999, -0, -0, -0, -0, -0, -0, -0, -0, -0], \
        [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [-0, -0, -1, -0, -0, -0, -0, -0, -0, -0, -0, -0, -0], \
        [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0], \
        [-0, -0, -0, -0, -1, -0, -0, -0, -0, -0, -0, -0, -0], \
        [0, 0, 0, -1, 0, 1, 0, 0, 0, 0, 0, 0, 0], \
        [-0, -0, -0, 1, -0, -1, -0, -0, -0, -0, -0, -0, -0], \
        [0, 0, 0, -12, 0, 0, 1, 0, 0, 0, 0, 0, 0], \
        [-0, -0, -0, 12, -0, -0, -1, -0, -0, -0, -0, -0, -0], \
        [0, 0, 0, -0.000060000000000000015, 0, 0, 0, 1, 0, 0, 0, 0, 0], \
        [-0, -0, -0, 0.000060000000000000015, -0, -0, -0, -1, -0, -0, -0, -0, -0], \
        [0, 0, 0, -1, 0, 0, 0, 0, 1, 0, 0, 0, 0], \
        [-0, -0, -0, 1, -0, -0, -0, -0, -1, -0, -0, -0, -0], \
        [0, 0, 0, -0.000060000000000000015, 0, 0, 0, 0, 0, 1, 0, 0, 0], \
        [-0, -0, -0, 0.000060000000000000015, -0, -0, -0, -0, -0, -1, -0, -0, -0], \
        [0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 1, 0, 0], \
        [-0, -0, -0, 1, -0, -0, -0, -0, -0, -0, -1, -0, -0], \
        [0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0], \
        [-0, -0, -0, -0, -0, -0, -0, -0, -0, -0, -0, -1, -0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], \
        [-0, -0, -0, -0, -0, -0, -0, -0, -0, -0, -0, -0, -1], ]
    rhs = [-0.06000000000000001, 0.06000000000000001, -25.009999999999998, 25.009999999999998, 0, -0, 0, -0, 0, -0, 0, -0, -0.0031200000000000004, 0.0031200000000000004, 0, -0, -0.0031200000000000004, 0.0031200000000000004, 0, -0, -27, 33, 0, -0, 1, -1, ]
    rv.append(StateSet(lputil.from_constraints(mat, rhs, mode), mode))
    
    return rv


def define_settings(image_path):
    '''get the hylaa settings object
    see hylaa/settings.py for a complete list of reachability settings'''

    # step_size = 5.0E-4, max_time = 2.0
    settings = HylaaSettings(5.0E-4, 2.0)
    settings.plot.plot_mode = PlotSettings.PLOT_NONE
    
    if image_path is not None:
        settings.plot.filename = image_path
        settings.plot.plot_mode = PlotSettings.PLOT_IMAGE
        settings.plot.xdim_dir = 0
        settings.plot.ydim_dir = 2

    settings.aggstrat=aggstrat.Unaggregated()

    return settings

def run_hylaa(image_path):
    'runs hylaa, returning a HylaaResult object'
    ha = define_ha()
    init = define_init_states(ha)
    settings = define_settings(image_path)

    result = Core(ha, settings).run(init)

    return result

def measure():
    'measure and add to results.csv'

    instance = '02'
    image_path = None
    
    if len(sys.argv) > 1 and sys.argv[1].endswith('.png'):
        image_path = sys.argv[1]

    start = time.perf_counter()
    result = run_hylaa(image_path)
    assert not result.has_concrete_error, "error mode was reachable"
    diff = time.perf_counter() - start

    if image_path is None:
        with open("../results.csv", "a") as f:
            f.write(f"Hylaa,DTN,{instance},1,{diff},\n")


if __name__ == '__main__':
    measure()
