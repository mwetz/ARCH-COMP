function plotSimulation(obj,varargin)
% plotSimulation - plots simulation results of a hybrid automaton
%
% Syntax:  
%    plotSimulation(obj)
%    plotSimulaiton(obj,dim)
%
% Inputs:
%    obj - hybrid automaton object
%
% Outputs:
%    none
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author: Matthias Althoff
% Written: 04-May-2007 
% Last update: 14-January-2008
% Last revision: ---

%------------- BEGIN CODE --------------

    % parse input arguments
    dim = [1,2];

    if nargin >= 2 && ~isempty(varargin{1})
       dim = varargin{1}; 
    end

    % load data from object structure
    t = obj.result.simulation.t;
    x = obj.result.simulation.x;

    % plot simulation results
    hold on

    for i=1:length(t)
        plot(x{i}(:,dim(1)),x{i}(:,dim(2)),'-k');
    end
end