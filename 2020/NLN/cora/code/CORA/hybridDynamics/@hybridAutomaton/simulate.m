function obj = simulate(obj,params,options)
% simulate - simulates a hybrid automaton
%
% Syntax:  
%    obj = simulate(obj,params,options)
%
% Inputs:
%    obj - hybrid automaton object
%    params - system parameters
%    options - simulation options
%
% Outputs:
%    obj - hybrid automaton object
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:        Matthias Althoff
% Written:       03-May-2007 
% Last update:   08-May-2020 (MW, update interface)
% Last revision: ---

%------------- BEGIN CODE --------------

params = checkOptionsSimulate(obj,params);

% initialization
tInter = params.tStart;    % intermediate time at transitions
loc = params.startLoc;     % current location
xInter = params.x0;        % intermediate state at transitions

locList = [];
t = {};
x = {};

% iteratively simulate for each location seperately
while (tInter < params.tFinal) && ...
      (~isempty(loc)) && ~isFinalLocation(loc,params.finalLoc)

    %store locations
    locList = [locList, loc];

    % choose input
    params.u = params.uLoc{loc};

    % simulate within the current location
    params.tStart = tInter;
    params.x0 = xInter;
    [tNew,xNew,loc,xInter] = simulate(obj.location{loc},params);

    % update time
    tInter=tNew(end);

    % store results
    t{end+1} = tNew;
    x{end+1} = xNew;
end

% save results to object structure
saved = 0;

if ~isfield(obj,'result')

    temp = obj.result;

    if ~isfield(temp,'simulation')
        obj.result.simulation.t = t;
        obj.result.simulation.x = x;
        obj.result.simulation.location = locList;
        saved = 1;
    end
end

if ~saved
    obj.result.simulation.t = [obj.result.simulation.t,t];
    obj.result.simulation.x = [obj.result.simulation.x,x];
    obj.result.simulation.location = ...
                              [obj.result.simulation.location,locList];
end

%------------- END OF CODE --------------