Instructions to obtain IsaVODEs results of the ARCH2022 competition:
1. Grant permissions to `RUNME.sh` script, that is type in your command line `$ chmod +x RUNME.sh`
2. Run the "RUNME.sh" script, that is type in your command line `$ ./RUNME.sh`

**DOCKER NOTES:** 
1. Isabelle needs to build huge dependencies for the competitions to be verified.
2. Building `HOL-Analysis` may take up to an hour and repeated attempts to build the docker image.
3. Building `Ordinary_Differential_Equations` may take up to 20 minutes.
4. After waiting for LaTeX and Isabelle to build in the container, open the formalisation of the problems in the Isabelle-generated "document.pdf".

**COMPETITION NOTES:** 
1. Each problem corresponds to a subsection of the pdf. 
2. Problems partially proved include the `sorry` command. 
3. Problems that we did not manage to tackle during the time frame of the competition include an `oops` command.