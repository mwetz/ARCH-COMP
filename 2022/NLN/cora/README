The repeatability package for the ARCH2022/NLN categoryy for the CORA toolbox
has been set up in CodeOcean (see https://codeocean.com/).
In CodeOcean, all results can be conveniently reproduced with a single click
on "Reproducible run", without requiring any more installations.

This directory contains the files for the exported CodeOcean capsule,
which enables the generation of the results using Docker.
For this, follow the following steps:

1. MAC-Address: Substitute the MAC address (now set to 00:00:00:00:00:00)
in the file "measure_all.sh" with the MAC address of your local machine.

2. MATLAB License: For the docker container to run MATLAB,
one has to create a new license file for the container.
Log in with your MATLAB account at https://www.mathworks.com/licensecenter/licenses/ .
Click on your license, and then navigate to
	> "Install and Activate"
	> "Activate to Retrieve License File"
	(...may differ depending on how your licensing is set up).
Create a new license file with the following data:
	- MATLAB version: 2019a, 
	- MAC address: MAC address of your local machine
	- User: root

Finally, download the generated file "licence.lic" and put it into the directory "NLN/cora/license"

3. Run the code: make the script "measure_all" executable by typing "chmod +x measure_all.sh" to the terminal.
Then execute the script by typing "./measure_all".
Equivalently, you make run the commands from "measure_all" directly.
