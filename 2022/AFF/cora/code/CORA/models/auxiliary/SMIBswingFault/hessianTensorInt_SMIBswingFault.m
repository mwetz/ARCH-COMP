function [Hf,Hg]=hessianTensorInt_SMIBswingFault(x,y,u)

% helper variables
cosy3x1pi2 = cos(y(3) - x(1) + pi/2);
y4cosy3x1pi2 = y(4)*cosy3x1pi2;
piy4cosy3x1pi2 = pi * y4cosy3x1pi2;
siny3x1pi2 = sin(y(3) - x(1) + pi/2);
pisiny3x1pi2 = pi * siny3x1pi2;
y4siny3x1pi2 = y(4) * siny3x1pi2;
siny3 = sin(y(3));
y4siny3 = y(4) * siny3;
cosy3 = cos(y(3));
y4cosy3 = y(4) * cosy3;
% factors
fac1 = 82.650027684953869; % = 372222633884119425/4503599627370496
fac2 = 5.510001845663592; % = 24814842258941295/4503599627370496
fac3 = 1.385861538461538; % = 90081/65000;
% factors * variables
fac1piy4cosy3x1pi2 = fac1*piy4cosy3x1pi2;
mfac1piy4cosy3x1pi2 = -fac1piy4cosy3x1pi2;
fac1pisiny3x1pi2 = fac1*pisiny3x1pi2;
mfac1pisiny3x1pi2 = -fac1pisiny3x1pi2;
fac2y4cosy3x1pi2 = fac2*y4cosy3x1pi2;
mfac2y4cosy3x1pi2 = -fac2y4cosy3x1pi2;
fac2siny3x1pi2 = fac2*siny3x1pi2;
mfac2siny3x1pi2 = -fac2siny3x1pi2;
fac2cosy3x1pi2 = fac2*cosy3x1pi2;
mfac2cosy3x1pi2 = -fac2cosy3x1pi2;
fac2y4siny3x1pi2 = fac2*y4siny3x1pi2;
mfac2y4siny3x1pi2 = -fac2y4siny3x1pi2;
fac3siny3 = fac3*siny3;
fac3cosy3 = fac3*cosy3;

intsparse = interval(sparse(7,7),sparse(7,7));
 Hf{1} = intsparse;



 Hf{2} = intsparse;

Hf{2}(1,1) = fac1piy4cosy3x1pi2;
Hf{2}(5,1) = mfac1piy4cosy3x1pi2;
Hf{2}(6,1) = mfac1pisiny3x1pi2;
Hf{2}(1,5) = mfac1piy4cosy3x1pi2;
Hf{2}(5,5) = fac1piy4cosy3x1pi2;
Hf{2}(6,5) = fac1pisiny3x1pi2;
Hf{2}(1,6) = mfac1pisiny3x1pi2;
Hf{2}(5,6) = fac1pisiny3x1pi2;


 Hg{1} = intsparse;

Hg{1}(1,1) = fac2y4cosy3x1pi2;
Hg{1}(5,1) = mfac2y4cosy3x1pi2;
Hg{1}(6,1) = mfac2siny3x1pi2;
Hg{1}(1,5) = mfac2y4cosy3x1pi2;
Hg{1}(5,5) = fac2y4cosy3x1pi2;
Hg{1}(6,5) = fac2siny3x1pi2;
Hg{1}(1,6) = mfac2siny3x1pi2;
Hg{1}(5,6) = fac2siny3x1pi2;


 Hg{2} = intsparse;

Hg{2}(1,1) = fac2y4siny3x1pi2;
Hg{2}(5,1) = mfac2y4siny3x1pi2;
Hg{2}(6,1) = fac2cosy3x1pi2;
Hg{2}(1,5) = mfac2y4siny3x1pi2;
Hg{2}(5,5) = fac2y4siny3x1pi2;
Hg{2}(6,5) = mfac2cosy3x1pi2;
Hg{2}(1,6) = fac2cosy3x1pi2;
Hg{2}(5,6) = mfac2cosy3x1pi2;
Hg{2}(6,6) = 10;


 Hg{3} = intsparse;

Hg{3}(5,5) = -fac3*y4siny3;
Hg{3}(6,5) = fac3cosy3;
Hg{3}(5,6) = fac3cosy3;


 Hg{4} = intsparse;

Hg{4}(5,5) = fac3*y4cosy3;
Hg{4}(6,5) = fac3siny3;
Hg{4}(5,6) = fac3siny3;
Hg{4}(6,6) = 40/13;
