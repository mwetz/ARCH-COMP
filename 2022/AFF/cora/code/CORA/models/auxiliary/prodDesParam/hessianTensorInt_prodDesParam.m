function Hf=hessianTensorInt_prodDesParam(x,u)



 Hf{1} = interval(sparse(5,5),sparse(5,5));

Hf{1}(1,1) = (2*x(2))/(x(1) + 1)^2 - (2*x(1)*x(2))/(x(1) + 1)^3;
Hf{1}(2,1) = x(1)/(x(1) + 1)^2 - 1/(x(1) + 1);
Hf{1}(1,2) = x(1)/(x(1) + 1)^2 - 1/(x(1) + 1);


 Hf{2} = interval(sparse(5,5),sparse(5,5));

Hf{2}(1,1) = (2*x(1)*x(2))/(x(1) + 1)^3 - (2*x(2))/(x(1) + 1)^2;
Hf{2}(2,1) = 1/(x(1) + 1) - x(1)/(x(1) + 1)^2;
Hf{2}(1,2) = 1/(x(1) + 1) - x(1)/(x(1) + 1)^2;
Hf{2}(4,2) = -1;
Hf{2}(2,4) = -1;


 Hf{3} = interval(sparse(5,5),sparse(5,5));

Hf{3}(4,2) = 1;
Hf{3}(2,4) = 1;


 Hf{4} = interval(sparse(5,5),sparse(5,5));

