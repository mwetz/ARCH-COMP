function p = randPoint(C,varargin)
% randPoint - Returns a random point of a capsule
%
% Syntax:  
%    p = randPoint(C)
%    p = randPoint(C,N)
%    p = randPoint(C,N,type)
%
% Inputs:
%    C - capsule object
%    N - number of random points
%    type - type of the random point ('standard' or 'extreme')
%
% Outputs:
%    p - random point inside the capsule
%
% Example: 
%    C = capsule([1; 1; 0], [0.5; -1; 1], 0.5);
%    p = randPoint(C)
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: zonotope/randPoint

% Author:       Mark Wetzlinger
% Written:      17-Sep-2019
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% parse input arguments
[N,type] = setDefaultValues({{1},{'standard'}},varargin{:});

% check input arguments
inputArgsCheck({{C,'att',{'capsule'},{''}};
                {N,'att',{'numeric'},{'positive','scalar','integer'}};
                {type,'str',{'standard','extreme'}}});

% dimension of capsule
n = dim(C);

% generate different types of extreme points
if strcmp(type,'standard')
    
    p = zeros(n,N);
    
    for i = 1:N
    
        dir = -1 + 2*rand(n,1);
        dir = dir / norm(dir,2);

        p(:,i) = C.c + (-1 + 2*rand)*C.g + dir * (rand*C.r); 
    end
    
elseif strcmp(type,'extreme')
    
    p = zeros(n,N);
    
    for i = 1:N
    
        dir = -1 + 2*rand(n,1);
        dir = dir / norm(dir,2);

        [~,x] = supportFunc(C,dir);
        
        p(:,i) = x; 
    end
    
else
    throw(CORAerror('CORA:wrongValue','third',"'standard' or 'extreme'"));
end

%------------- END OF CODE --------------