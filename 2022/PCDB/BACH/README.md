# Prerequisite

+ Docker should be installed.
	
# How to Run Benchmark Instances

```
bash measure_all
```

# Result

The results of *BACH-mixed* and *BACH-interaction* will be stored in `.result/result_mixed.csv` and `.result/result_interaction.csv` file.

## Format

|             Tool             |      Model      |      Specification      |      Verification Result      | Time (s) | Bound |
| :--------------------------: | :-------------: | :---------------------: | :---------------------------: | :------: | :---: |
| BACH-interaction/ BACH-mixed | Model directory | Specification directory | Reachable (R)/ Unreachable(U) |    -     |   -   |

