classdef batchCombinator < handle
% batchCombinator - Object which generates a sampling without repetitions/replacement of the set 1:N, taken K at a time 
%                   Can be called batch-wise to enable large outputs
%                   Based on the combinator by Matt Fig
%
% Syntax:
%    obj = batchCombinator(N, K)
%
% Inputs:
%    N - sample set size
%    K - sample drawn at a time. Requirement: K<=M
%
% Outputs:
%    obj - generated batchCombinator object
%
% Example: 
%    combinator = batchCombinator(10, 5);
%    first_batch = combinator.nextBatch(1000);
%    while combinator.done == false
%       new_batch = combinator.nextBatch(1000);
%    end
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: ---

% Author:       Michael Eichelbeck
% Written:      11-July-2022 
% Last update:  ---
% Last revision: ---

%------------- BEGIN CODE --------------
    
    properties
        N
        K
        M
        WV
        lim
        inc
        stp
        flg
        BC
        ii
        done
    end
    
    methods

        function obj = batchCombinator(N, K)
            
            obj.N = N;
            obj.K = K;

            if K>N
                error(['K must be less than or equal to N'])
            end
        
            obj.M = double(N);  % Single will give us trouble on indexing.
            
            obj.WV = 1:K;  % Working vector.
            obj.lim = K;   % Sets the limit for working index.
            obj.inc = 1;   % Controls which element of WV is being worked on.
            obj.stp = 0;   % internal tracker
            obj.flg = 0;   % internal tracker
            
            obj.BC = floor(prod(obj.M-obj.K+1:obj.M) / (prod(1:obj.K)));  % total number of combinations
            obj.ii = 1; % global index

            obj.done = false; % have all combinations been returned?

        end

        function CN = nextBatch(obj, n)
            
            % copy properties to local variables for speed
            N = obj.N;
            K = obj.K;
            M = obj.M;
            WV = obj.WV;
            lim = obj.lim;
            inc = obj.inc;
            stp = obj.stp;
            flg = obj.flg;
            BC = obj.BC;
            ii = obj.ii;
            done = obj.done;

            if ii + n > BC % last batch
                n = BC-(ii-1);
                n = floor(n);
            end

            CN = zeros(n,K,class(N));
            
            for nii = 1:n
            
                if ii == 1  % The first row. 
                    CN(nii,:) = WV;             
                elseif ii == BC % The last row. 
                    CN(nii,:) = (N-K+1):N;
                    done = true;
                
                else % all other rows
    
                    if logical((inc+lim)-N) % The logical is nec. for class single(?)
                        stp = inc;  % This is where the for loop below stops.
                        flg = 0;  % Used for resetting inc.
                    else
                        stp = 1;
                        flg = 1;
                    end
                    
                    for jj = 1:stp
                        WV(K  + jj - inc) = lim + jj;  % Faster than a vector assignment.
                    end
                    
                    CN(nii,:) = WV;  % Make assignment.
                    inc = inc*flg + 1;  % Increment the counter.
                    lim = WV(K - inc + 1 );  % lim for next run.     
                
                end % end if
                
                ii = ii + 1;

            end % end for

            % copy local variables to instance properties
            obj.N = N;
            obj.K = K;
            obj.M = M;
            obj.WV = WV;
            obj.lim = lim;
            obj.inc = inc;
            obj.stp = stp;
            obj.flg = flg;
            obj.BC = BC;
            obj.ii = ii;
            obj.done = done;
                  
        end % end of function

    end % end methods

end % end class

%------------- END CODE --------------