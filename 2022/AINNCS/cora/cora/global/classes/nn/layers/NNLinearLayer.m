classdef NNLinearLayer < NNLayer
    % NNLinearLayer - class for linear layers
    %
    % Syntax:
    %    obj = NNLinearLayer(W, b, name)
    %
    % Inputs:
    %    W - weight matrix
    %    b - bias column vector
    %    name - name of the layer, defaults to type
    %
    % Outputs:
    %    obj - generated object
    %
    % Other m-files required: none
    % Subfunctions: none
    % MAT-files required: none
    %
    % See also: NeuralNetwork
    %
    % Author:       Tobias Ladner
    % Written:      28-March-2022
    % Last update:  ---
    % Last revision:---

    %------------- BEGIN CODE --------------

    properties (Constant)
        type = "LinearLayer"
        is_refinable = false
    end

    properties
        W, b
    end

    methods
        % constructor
        function obj = NNLinearLayer(W, b, name)
            if nargin < 3
                name = NNLinearLayer.type;
            end
            % call super class constructor
            obj@NNLayer(name)

            obj.W = W;
            obj.b = b;
        end

        function [nin, nout] = getNumNeurons(obj)
            nin = size(obj.W, 2);
            nout = size(obj.W, 1);
        end

        % evaluate
        function r = evaluateNumeric(obj, input)
            r = obj.W * input + obj.b;
        end

        function r = evaluateZonotope(obj, Z, evParams)
            Z = obj.W * Z;
            Z(:, 1) = Z(:, 1) + obj.b;
            r = Z;
        end

        function [c, G, Grest, expMat, id, id_, ind, ind_] = evaluatePolyZonotope(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
            c = obj.W * c + obj.b;
            G = obj.W * G;
            Grest = obj.W * Grest;
        end

        function r = evaluateTaylm(obj, input, evParams)
            r = obj.W * input + obj.b;
        end

        function [c, G, C, d, l, u] = evaluateConZonotope(obj, c, G, C, d, l, u, options, evParams)
            c = obj.W * c + obj.b;
            G = obj.W * G;
        end

        function S = evaluateSensitivity(obj, S, x)
            S = S * obj.W;
        end
    end
end