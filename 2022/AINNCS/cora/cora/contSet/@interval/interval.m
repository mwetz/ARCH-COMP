classdef (InferiorClasses = {?mp}) interval < contSet
% interval - object constructor for real-valued intervals 
%
% Description:
%    This class represents interval objects defined as
%    {x | a_i <= x <= b_i, \forall i = 1,...,n}.
%
% Syntax:
%    obj = interval()
%    obj = interval(I)
%    obj = interval(a)
%    obj = interval(a,b)
%
% Inputs:
%    I - interval object
%    a - lower limit
%    b - upper limit
%
% Outputs:
%    obj - generated interval object
%
% Example:
%    a = [1;-1];
%    b = [2;3];
%    I = interval(a,b);
%    plot(I,[1,2],'r');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: interval, polytope

% Author:       Matthias Althoff, Niklas Kochdumper
% Written:      19-June-2015
% Last update:  18-November-2015
%               26-January-2016
%               15-July-2017 (NK)
%               01-May-2020 (MW, delete redundant if-else)
%               20-March-2021 (MW, errConstructor)
% Last revision:---

%------------- BEGIN CODE --------------

properties (SetAccess = private, GetAccess = public)
    inf (:,:) double {mustBeNumeric} = [];
    sup (:,:) double {mustBeNumeric} = [];
end

methods
    %class constructor
    function obj = interval(varargin)
        
        % one input argument
        if nargin==1
            
            if isa(varargin{1},'interval')
                % copy constructor
                obj = varargin{1};
            elseif isnumeric(varargin{1})
                obj.inf = varargin{1};
                obj.sup = varargin{1};
            else
                throw(CORAerror('CORA:wrongValue','first',...
                    "'interval' object or a vector"));
            end
        
        % two input arguments
        elseif nargin==2
            
            % check sizes
            if any(any(size(varargin{1}) - size(varargin{2})))
                throw(CORAerror('CORA:wrongInputInConstructor',...
                    'Limits are of different dimension.'));
            elseif all(all(varargin{1} <= varargin{2}))
                obj.inf = varargin{1};
                obj.sup = varargin{2};
            else
                 throw(CORAerror('CORA:wrongInputInConstructor',...
                     'Lower limit larger than upper limit.'));
            end
        
        elseif nargin > 2
            
            % too many input arguments
            throw(CORAerror('CORA:tooManyInputArgs',2));
            
        end
        
        % set parent object properties
        obj.dimension = size(obj.inf,1);
    end
    
    function ind = end(obj,k,n)
    % overloads the end operator for referencing elements, e.g. I(end,2),
        ind = size(obj,k);
    end
    
    % methods in seperate files
    res = abs(I) % absolute value function
    I = acos(I) % inverse cosine function
    I = acosh(I) % inverse hyperbolic cosine function
    res = and(I,S) % intersection
    I = asin(I) % inverse sine function
    I = asinh(I) % inverse hyperbolic sine function
    I = atan(I) % inverse tangent function
    I = atanh(I) % inverse hyperbolic tangent function
    C = capsule(I) % conversion to capsule object
    res = cartProd(I,S) % Cartesian product
    c = center(I) % center of interval
    cPZ = conPolyZono(I) % conversion to conPolyZono object
    res = convHull(I,varargin) % convex hull
    cZ = conZonotope(I) % conversion to conZonotope object
    res = cos(I) % cosine function
    I = cosh(I) % hyperbolic cosine function
    I = ctranspose(I) % overloaded ' operator
    res = diag(I) % overloaded diag-function
    n = dim(I) % dimension of interval
    E = ellipsoid(I) % conversion to ellipsoid object
    I = enlarge(I,factor) % enlargement by factor
    res = eq(I1,I2) % equality check
    I = exp(I) % overloaded exp-function
    p = gridPoints(I,segments) % generate grid points
    I = horzcat(varargin) % overloaded horizontal concatenation
    res = in(I,S) % containment check
    res = infimum(I) % read lower limit
    res = isempty(I) % empty object check
    res = isequal(I1,I2,varargin) % equal objects check
    res = isFullDim(I) % full dimensionality check
    res = isIntersecting(I,S,varargin) % intersection check
    res = isscalar(I) % one-dimensionality check
    res = le(I1,I2) % subseteq check
    l = length(I) % largest dimension of interval
    I = log(I) % logarithm function
    res = lt(I1,I2) % subset check
    I = minkDiff(I,S,varargin) % Minkowski difference
    res = minus(minuend,subtrahend) % overloaded - operator (binary)
    res = mpower(base,exponent) % overloaded ^ operator
    P = mptPolytope(I) % conversion to mptPolytope object
    res = mrdivide(numerator,denominator) % overloaded / operator
    res = mtimes(factor1,factor2) % overloaded * operator
    res = ne(I1,I2) % overloaded ~= operator
    val = norm(I,type) % norm function
    res = or(I,S) % union
    dzNew = partition(I, splits) % partition into subintervals
    han = plot(I,varargin) % plot
    res = plus(summand1,summand2) % overloaded + operator
    P = polytope(I,varargin) % conversion to polytope object
    pZ = polyZonotope(I) % conversion to polyZonotope object
    res = power(base,exponent) % overloaded .^ operator
    res = prod(I,varargin) % overloaded prod-function
    I = project(I,dims) % projection onto subspace
    I = quadMap(varargin) % quadratic map
    r = rad(I) % radius (half of diameter)
    r = radius(I) % radius of enclosing hyperball
    p = randPoint(I,varargin) % random point
    res = rdivide(numerator, denominator) % overloaded ./ operator
    I = reshape(I,varargin) % overloaded reshape-function
    res = sin(I) % sine function
    I = sinh(I) % hyperbolic sine function
    varargout = size(I, varargin) % overloaded size-function
    res = split(I,n) % split along one dimension
    I = sqrt(I) % square root
    I = subsasgn(I,S,val) % value assignment
    newObj = subsref(I,S) % read from object
    res = sum(I,varargin) % overloaded sum-function
    [val,x] = supportFunc(I,dir,varargin) % support function evaluation
    res = supremum(I) % read upper limit
    res = tan(I) % tangent function
    I = tanh(I) % hyperbolic tangent function
    res = times(factor1,factor2) % overloaded .* function
    I = transpose(I) % overloaded .' function
    I = uminus(I) % overloaded unary - operator
    I = uplus(I) % overloaded unary + operator
    I = vertcat(varargin) % vertical concantenation
    V = vertices(I) % vertices
    V = volume(I) % volume
    zB = zonoBundle(I) % conversion to zonoBundle object
    Z = zonotope(I) % conversion to zonotope object
    
    % display functions
    display(I)
end

methods (Static = true)
    I = generateRandom(varargin) % generates random interval
    I = enclosePoints(points) % enclosure of point cloud
end


end

%------------- END OF CODE -------