function corrTraj = errorSearch(fullTraj)
% errorSearch - checks whether the distances between body joints
% stay constant within a certain error margin. 
% 
%
% Syntax:  
%    traj_corrected = findTrackingErrors(traj,correctionFlag)
%
% Inputs:
%    traj - recorded trajectory of a marker
%    correction_flag - 1 if trajectory should be automatically
%
% Outputs:
%    t_violation - time relative to the start time of first violation; inf
%    is returned if no violation took place.
%
% Example: 
%
% 
% Author:       Matthias Althoff
% Written:      08-March-2019
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------

% obtain incidence matrix
incMat = incidenceMatrix();

% remove tracking errors
corrTraj = removeTrackingErrors(fullTraj, incMat);

% remove high frequencies
corrTraj = removeHighFrequencies(corrTraj);


%------------- END OF CODE --------------