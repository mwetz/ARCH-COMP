function osc_plot(mode,x0,nofControllers)
    % path of the SymbolicSet.m and the associated mex-file
    addpath(genpath('../../../'));
    % read data from the input file
    name = 'osc';
    [sDIM,~,W_ub,lb,ub,tau] = readParams('input.hh');
    switch mode
        case 'domain'
            co = SymbolicSet([name '_controller_over.bdd']);
            cu = SymbolicSet([name '_controller_under.bdd']);
            cw = SymbolicSet([name '_controller_wc.bdd']);
            
            figure;
            axis(reshape([lb'; ub'],[1 2*sDIM]));
            hold on;
            try
                po = co.points;
                plot(po(:,1),po(:,2),'.');
            catch
                warning('Points could no be loaded from controller_over.bdd\n');
            end
            try
                pu = cu.points;
                plot(pu(:,1),pu(:,2),'.','Color',[0.7 0.7 0.7]);
            catch
                warning('Points could no be loaded from controller_under.bdd\n');
            end
            try
                pw = cw.points;
                plot(pw(:,1),pw(:,2),'.');
            catch
                warning('Points could no be loaded from controller_wc.bdd\n');
            end
            savefig('osc_domain');
        case 'simulate_uniform_noise'
            controller=SymbolicSet('osc_controller_under.bdd');
            try
                controller.getInputs(x0);
            catch
                error('osc_plot:simulate_uniform_noise:initial state out of the winning region.');
            end
            openfig('osc_domain');
            axis(reshape([lb'; ub'],[1 2*sDIM]));
            hold on
            % load the symbolic set containing the controller
            target=SymbolicSet('osc_target_under.bdd');
            t=target.points;
            plot(t(:,1),t(:,2),'.','color',[0.698 1 0.4]);
            y=x0;
            % simulation horizon
            T=1000;
            for t=1:T
              x = oscillator(y(end,:),'uniform_stochastic'); 
              y=[y; x(end,:)];
              plot(y(end-1:end,1),y(end-1:end,2),'r.-');
              pause(0.05)
            end
            savefig('osc_traj_uniform')
        case 'simulate_normal_noise'
            controller=SymbolicSet('osc_controller_under.bdd','projection',[1 2]);
            try
                controller.getInputs(x0);
            catch
                error('osc_plot:simulate_normal_noise:initial state out of the winning region.');
            end
            openfig('osc_domain');
            axis(reshape([lb'; ub'],[1 2*sDIM]));
            hold on
            % load the symbolic set containing the controller
            target=SymbolicSet('osc_target_under.bdd');
            t=target.points;
            plot(t(:,1),t(:,2),'.','color',[0.698 1 0.4]);
            y=x0;
            % simulation horizon
            T=1000;
            for t=1:T
              x = oscillator(y(end,:),'normal_stochastic'); 
              y=[y; x(end,:)];
              plot(y(end-1:end,1),y(end-1:end,2),'r.-');
              pause(0.05)
            end
            savefig('osc_traj_normal');
        case 'simulate_wc_noise'
            controller=SymbolicSet('osc_controller_wc.bdd','projection',[1 2]);
            try
                controller.getInputs(x0);
            catch
                warning('osc_plot:simulate_normal_noise:initial state out of the winning region of the worst case controller.');
            end
            openfig('osc_traj_uniform');
            axis(reshape([lb'; ub'],[1 2*sDIM]));
            hold on
            % load the symbolic set containing the controller
            target=SymbolicSet('osc_target_under.bdd');
            t=target.points;
            plot(t(:,1),t(:,2),'.','color',[0.698 1 0.4]);
            y=x0;
            % simulation horizon
            T=1000;
            for t=1:T
              x = oscillator(y(end,:),'worst_case'); 
              y=[y; x(end,:)];
              plot(y(end-1:end,1),y(end-1:end,2),'k.-');
              pause(0.05)
            end
            savefig('osc_traj_uniform+wc');
        case 'interim_sets_under'
            for ii=1:nofControllers
                figure
                axis(reshape([lb'; ub'],[1 2*sDIM]));
                hold on
                jj = 1;
                while 1
                    try
                        set = SymbolicSet(['./IntSetsUnder/P' num2str(ii) '/Q' num2str(jj) '.bdd']);
                    catch
                        break;
                    end
                    p = set.points;
                    plot(p(:,1),p(:,2),'.');
                    pause(0.05)
                    jj = jj+1;
                end
            end
        case 'interim_sets_over'
            for ii=1:nofControllers
                figure
                axis(reshape([lb'; ub'],[1 2*sDIM]));
                hold on
                jj = 1;
                while 1
                    try
                        set = SymbolicSet(['./IntSetsOver/P' num2str(ii) '/Q' num2str(jj) '.bdd']);
                    catch
                        break;
                    end
                    p = set.points;
                    plot(p(:,1),p(:,2),'.');
                    pause(0.05)
                    jj = jj+1;
                end
            end
    end
    
    function xn = oscillator(x,noise_type)
        xn = zeros(size(x));
        % rotation by -45 degree
        a = [0.5 0.5; -0.5 0.5]*x';
        switch noise_type
            case 'uniform_stochastic'
                % uniformly distributed noise within the interval [-W_ub,W_ub]
                d = [2*W_ub(1)*rand-W_ub(1), 2*W_ub(2)*rand-W_ub(2)]; % uniform disturbance
            case 'normal_stochastic'
                % truncated to the interval [-W_ub,W_ub]
                d = [max(min(randn,W_ub(1)),-W_ub(1)), max(min(randn,W_ub(2)),-W_ub(2))]; % truncated normal distribution
            case 'worst_case'
                % some handpicked values
                d = [W_ub(1), -W_ub(2)]; % constant disturbance
        end
        xn(1) = a(1) + tau*a(2) + d(1);
        xn(2) = a(2) + tau*(-a(1)+(1-a(1)^2)*a(2)) + d(2);
        % rotation by 45 degree
        xn = ([1 -1;1 1]*xn')';
    end

end