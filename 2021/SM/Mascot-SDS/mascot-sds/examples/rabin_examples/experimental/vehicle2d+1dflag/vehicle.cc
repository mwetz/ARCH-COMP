/*
 * vehicle.cc
 *
 *  created on: 01.02.2021
 *      author: kaushik
 */

/*
 * dubin's vehicle (sampled time) with bounded external noise
 *
 */

#include <array>
#include <iostream>
#include <math.h>

#include "cuddObj.hh"

#include "SymbolicSet.hh"
// #include "SymbolicModelGrowthBound.hh"
#include "SymbolicModelMonotonic.hh"

#include "TicToc.hh"
#include "RungeKutta4.hh"
#include "FixedPointRabin.hh"
#include "input.hh"


/****************************************************************************/
/* main computation */
/****************************************************************************/
int main() {
    /* to measure time */
    TicToc timer;
    /* there is one unique manager to organize the bdd variables */
    Cudd mgr;
    /* create a log file */
    std::freopen("vehicle.log", "w", stderr);
    std::clog << "vehicle.log" << '\n';
    
    /****************************************************************************/
    /* construct SymbolicSet for the state space */
    /****************************************************************************/
    /* setup the workspace of the synthesis problem and the uniform grid */
    /* the varaibles lb, ub and eta are loaded from input.hh */
    scots::SymbolicSet ss(mgr,sDIM,lb,ub,eta);
    ss.addGridPoints();
    /****************************************************************************/
    /* construct SymbolicSet for the input space */
    /****************************************************************************/
    /* the varaibles ilb, iub and ieta are loaded from input.hh */
    scots::SymbolicSet is(mgr,iDIM,ilb,iub,ieta);
    is.addGridPoints();
    
    /****************************************************************************/
    /* setup class for symbolic model computation */
    /****************************************************************************/
    scots::SymbolicSet sspost(ss,1); /* create state space for post variables */
    scots::SymbolicModelMonotonic<state_type,input_type> abstraction(&ss, &is, &sspost);
    if (RFF) {
        std::cout << "Reading transition relations from file." << '\n';
        scots::SymbolicSet mt(mgr,"maybeTransitions.bdd");
        scots::SymbolicSet st(mgr,"sureTransitions.bdd");
        abstraction.setTransitionRelations(mt.getSymbolicSet(),st.getSymbolicSet());
        /* get the number of elements in the transition relation */
        std::cout << std::endl << "Number of elements in the maybe transition relation: " << abstraction.getSizeMaybe() << std::endl;
        std::cout << std::endl << "Number of elements in the sure transition relation: " << abstraction.getSizeSure() << std::endl;
    } else {
        /* compute the transition relation */
        timer.tic();
        abstraction.computeTransitions(decomposition, W_ub);
        std::cout << std::endl;
        double time_abstraction = timer.toc();
        std::clog << "Time taken by the abstraction computation: " << time_abstraction << "s.\n";
        /* get the number of elements in the transition relation */
        std::cout << std::endl << "Number of elements in the maybe transition relation: " << abstraction.getSizeMaybe() << std::endl;
        std::cout << std::endl << "Number of elements in the sure transition relation: " << abstraction.getSizeSure() << std::endl;
        /* save transition relations */
        (abstraction.getMaybeTransitions()).writeToFile("maybeTransitions.bdd");
        (abstraction.getSureTransitions()).writeToFile("sureTransitions.bdd");
    }
    /* The 5-state rabin automaton */
    /* the inputs:
        0-> kitchen & no-request,
        1-> kitchen & request,
        2-> office & no-request,
        2-> office & request,
        3-> no-kitchen & no-office & request,
        4-> other (everything else)  */
    scots::SymbolicSet kitchen(sspost), office(sspost), req(sspost), no_req(sspost), others(sspost);
    scots::SymbolicSet i0(sspost), i1(sspost), i2(sspost), i3(sspost), i4(sspost), i5(sspost);
//    scots::SymbolicSet kitchen(ss), office(ss), req(ss), others(ss);
    kitchen.addPolytope(4,H2,g2,scots::INNER);
    office.addPolytope(4,H1,g1,scots::INNER);
    req.addPolytope(2,Hr,r1,scots::OUTER);
    no_req.addPolytope(2,Hr,r2,scots::OUTER);
    others.addGridPoints();
    others.setSymbolicSet(others.getSymbolicSet() & (no_req.getSymbolicSet()) & (!kitchen.getSymbolicSet()) & (!office.getSymbolicSet()));
    i0.setSymbolicSet(kitchen.getSymbolicSet() & (no_req.getSymbolicSet()));
    i1.setSymbolicSet(kitchen.getSymbolicSet() & req.getSymbolicSet());
    i2.setSymbolicSet(office.getSymbolicSet() & (no_req.getSymbolicSet()));
    i3.setSymbolicSet(office.getSymbolicSet() & req.getSymbolicSet());
    i4.setSymbolicSet((!kitchen.getSymbolicSet()) & (!office.getSymbolicSet()) & req.getSymbolicSet());
    i5.setSymbolicSet(others.getSymbolicSet());
    std::vector<BDD> rabin_inputs = {
        i0.getSymbolicSet(),
        i1.getSymbolicSet(),
        i2.getSymbolicSet(),
        i3.getSymbolicSet(),
        i4.getSymbolicSet(),
        i5.getSymbolicSet()
    };
    /* save rabin inputs */
    helper::checkMakeDir("rabin_inputs");
    i0.writeToFile("rabin_inputs/i0.bdd");
    i1.writeToFile("rabin_inputs/i1.bdd");
    i2.writeToFile("rabin_inputs/i2.bdd");
    i3.writeToFile("rabin_inputs/i3.bdd");
    i4.writeToFile("rabin_inputs/i4.bdd");
    i5.writeToFile("rabin_inputs/i5.bdd");
    no_req.writeToFile("rabin_inputs/no_req.bdd");
    /* the transitions */
    std::vector<std::array<size_t,3>> rabin_transitions ={
        {0, 0, 1},
        {0, 1, 2},
        {0, 2, 0},
        {0, 3, 2},
        {0, 4, 2},
        {0, 5, 0},
//
        {1, 0, 1},
        {1, 1, 2},
        {1, 2, 0},
        {1, 3, 2},
        {1, 4, 2},
        {1, 5, 0},
//
//        {1, 0, 1},
//        {1, 1, 1},
//        {1, 2, 1},
//        {1, 3, 1},
//        {1, 4, 1},
//        {1, 5, 1},
//
        {2, 0, 2},
        {2, 1, 2},
        {2, 2, 3},
        {2, 3, 3},
        {2, 4, 2},
        {2, 5, 2},
//
        {3, 0, 4},
        {3, 1, 4},
        {3, 2, 3},
        {3, 3, 3},
        {3, 4, 3},
        {3, 5, 3},
//
        {4, 0, 0},
        {4, 1, 0},
        {4, 2, 0},
        {4, 3, 0},
        {4, 4, 0},
        {4, 5, 0}
    };
    std::vector<std::array<std::vector<size_t>,2>> rabin_pairs;
    std::vector<size_t> v1, v2;
    std::array<std::vector<size_t>,2> pair;
    v1= {0};
    v2 = {};
    pair = {v1,v2};
    rabin_pairs.push_back(pair);
    mascot::RabinAutomaton rabin(mgr, 5, sspost, rabin_inputs, rabin_transitions, rabin_pairs);
    
    /* we setup a fixed point object to compute reach and stay controller */
    scots::FixedPointRabin fp(&abstraction,&rabin);
    std::cout << "FixedPoint initialized." << '\n';
    
    /****************************************************************************/
    /* Under-approximation of almost sure winning region */
    /****************************************************************************/
    std::cout << "\n\n Starting computation of under-approximation of the a.s. controller.\n";
    timer.tic();
    std::vector<BDD> CU = fp.Rabin("under",&rabin,verbose);
    helper::checkMakeDir("controllers");
    for (size_t i=1; i<=CU.size(); i++) {
        scots::SymbolicSet controller_under(ss,is);
        controller_under.setSymbolicSet(CU[i-1]);
        std::string Str = "";
        Str += "controllers/C";
        Str += std::to_string(i);
        Str += ".bdd";
        char Char[20];
        size_t Length = Str.copy(Char, Str.length() + 1);
        Char[Length] = '\0';
        controller_under.writeToFile(Char);
    }
    /* save the rabin transition function */
    rabin.writeToFile("rabin");
    double time_under_approx = timer.toc();
    std::clog << "Time taken by the under-approximation computation: " << time_under_approx << "s.\n";
    
    return 1;
}
