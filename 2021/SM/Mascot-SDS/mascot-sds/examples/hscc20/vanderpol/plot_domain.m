%
%   plot_domain.m
%
%   created on: 01.01.2020
%       author: kaushik
%
%   this program plots the over and under-approximation of winning region
%
%   you need to run ./osc binary first
%   so that the controller bdds are generated
%   

% path of the SymbolicSet.m and the associated mex-file
addpath(genpath('../../../'));

% load the controllers
co = SymbolicSet('osc_winning_over.bdd');
cu = SymbolicSet('osc_controller_under.bdd');
cw = SymbolicSet('osc_controller_wc.bdd');

% state space bounds 
[sDIM,~,~,lb,ub,~] = readParams('input.hh');

% prepare the figure window
figure;
axis(reshape([lb'; ub'],[1 2*sDIM]));
hold on;

% plot the over-approximation
try
    po = co.points;
    plot(po(:,1),po(:,2),'.','MarkerFaceColor',[0 0.4470 0.7410],'MarkerEdgeColor',[0 0.4470 0.7410]);
catch
    warning('Points could not be loaded from controller_over.bdd\n');
end

% plot the under-approximation
try
    pu = cu.points;
    plot(pu(:,1),pu(:,2),'.','MarkerFaceColor',[0.7 0.7 0.7],'MarkerEdgeColor',[0.7 0.7 0.7]);
catch
    warning('Points could no be loaded from controller_under.bdd\n');
end

% plot the worst case controller domain
try
    pw = cw.points;
    plot(pw(:,1),pw(:,2),'.','MarkerFaceColor','r','MarkerEdgeColor','r');
catch
    warning('Points could no be loaded from controller_wc.bdd\n');
end
savefig('osc_domain');