/*
 * RabinAutomaton.hh
 *
 *  created on: 28.01.2021
 *      author: kaushik mallik
 *
 */

#ifndef RABINAUTOMATON_HH_
#define RABINAUTOMATON_HH_

#include <stdlib.h>
#include <math.h>
//#include <vector>

#include "dddmp.h"
#include "cuddObj.hh"
#include "Helper.hh"

/* class: RabinAutomaton
 *
 * symbolic (bdd) implementation of a Rabin automaton
 *
 */
namespace mascot {

/* structure containing a single rabin pair */
struct rabin_pair_ {
    size_t rabin_index_; /* each rabin pair gets a unique index */
    BDD G_; /* the states which are to be visited infinitely often */
    BDD nR_; /* the complement of the states which are to be visited finitely often */
};

class RabinAutomaton {
public:
    /* var: ddmgr_
     *  the bdd manager */
    Cudd *ddmgr_;
    /* var: stateSpace_ */
    scots::SymbolicSet* stateSpace_;
    /* var: inputSpace_ */
    scots::SymbolicSet* inputSpace_;
    /* var: stateSpacePost_ */
    scots::SymbolicSet* stateSpacePost_;
    /* var: transitions_
     *  the bdd representing the transition relation as sets of tuples (s,x',s'), where s is pre-state of the rabin automaton, x' is post-state of the symbolic model, and s' is the post-state of the rabin automaton */
    BDD transitions_;
    /* var: numRabinPairs_
     *  the number of rabin pairs */
    size_t numRabinPairs_;
    /* var: RabinPairs_
     *  BDD vector[numRabinPairs_][2] containing the rabin pairs {(G_i,~R_i)}, where the Rabin condition is given as:
        \/_i ([]<>G_i & <>[]~R_i)
     * and each G_i,~R_i are given a bdd representing a set of states of the automaton */
    std::vector<rabin_pair_> RabinPairs_;
    friend class FixedPointGR1;
    friend class FixedPointRabin;
public:
    /* constructor: RabinAutomaton
     * constructs the bdd representation of the Rabin automaton from a given list representation of the states and the transitions
     *
     * Input:
     * ddmgr    - the cudd manager
     * ns       - number of states
     * inputSpace - symbolic set representing the input space
     * inputs   - vector containing the inputs; index should match with the transitions (predicate over the post states of the symbolic model)
     * transitions - vector containing the (directed) transitions (as arrays of size 3, containing the source vertex, the input, and the end vertex)
     * RabinPairs - vector containing the list of Rabin pairs (as pairs of Gi,Ri, where each Gi and Ri are vectors of state indices) */
    RabinAutomaton(Cudd &ddmgr,
                   const size_t ns,
                   const scots::SymbolicSet& inputSpace,
                   const std::vector<BDD> inputs,
                   const std::vector<std::array<size_t,3>> transitions,
                   const std::vector<std::array<std::vector<size_t>,2>> RabinPairs) {
        ddmgr_=&ddmgr;
        /* create a symbolic set representing the state space of the rabin automaton
         * the symbolic set is created by discretizing the range [1,ns] on the real line with eta=1 */
        size_t sdim=1; /* we model the states as discrete points on the real line */
        double slb[1] = {0};
        double sub[1] = {static_cast<double>(ns-1)};
        double seta[1] = {1};
        int grid_alignment = 0; /* the origin is a grid point */
        stateSpace_ = new scots::SymbolicSet(*ddmgr_, sdim, slb, sub, seta, grid_alignment);
        inputSpace_ = new scots::SymbolicSet(inputSpace);
        stateSpacePost_= new scots::SymbolicSet(*stateSpace_,1);
        /* build the transitions bdd */
        transitions_=ddmgr_->bddZero();
        for (size_t i=0; i<transitions.size(); i++) {
            BDD cube=ddmgr_->bddOne();
            std::vector<size_t> v;
            v={transitions[i][0]};
            cube &= stateSpace_->elementToMinterm(v);
            cube &= inputs[transitions[i][1]];
            v={transitions[i][2]};
            cube &= stateSpacePost_->elementToMinterm(v);
            transitions_ |= cube;
        }
        /* build the rabin pairs in terms of the pre variables */
        numRabinPairs_=RabinPairs.size();
        for (size_t i=0; i<RabinPairs.size(); i++) {
            rabin_pair_ pair;
            pair.rabin_index_=i;
            pair.G_=ddmgr_->bddZero();
            pair.nR_=ddmgr_->bddZero();
            std::vector<size_t> v;
            /* first, create the BDD for the G sets */
            for (size_t j=0; j<RabinPairs[i][0].size(); j++) {
                v={RabinPairs[i][0][j]};
                pair.G_ |= stateSpace_->elementToMinterm(v);
            }
            /* second, create the BDD for the COMPLEMENT OF the R sets */
            for (size_t j=static_cast<size_t>(stateSpace_->getFirstGridPoint()[0]); j<=static_cast<size_t>(stateSpace_->getLastGridPoint()[0]); j++) {
                bool isInR=false;
                for (size_t k=0; k<RabinPairs[i][1].size(); k++) {
                    if (RabinPairs[i][1][k] == j) {
                        isInR=true;
                        break;
                    }
                }
                if (!isInR) {
                    v={j};
                    pair.nR_ |= stateSpace_->elementToMinterm(v);
                }
            }
            RabinPairs_.push_back(pair);
        }
    }
    /* function:  writeToFile
     * save the transitions and the rabin pairs */
    void writeToFile(const char *foldername) {
        helper::checkMakeDir(foldername);
        /* create SymbolicSet representing the post stateSpace_*/
        /* make a copy of the SymbolicSet of the preSet */
        scots::SymbolicSet post(*stateSpacePost_);
//        /* set the BDD indices to the BDD indices used for the post stateSpace_ */
//        post.setIndBddVars((stateSpacePost_->getIndBddVars())[0]);
        /* create SymbolicSet representing the stateSpace_ x inputSpace_ */
        scots::SymbolicSet sis(*stateSpace_,*inputSpace_);
        /* create SymbolicSet representing the stateSpace_ x inputSpace_ x stateSpace_ */
        scots::SymbolicSet tr(sis,post);
        /* fill SymbolicSet with transition relation */
        tr.setSymbolicSet(transitions_);
        /* write the SymbolicSet to the file */
        std::string pathname=foldername;
        pathname += "/transitions.bdd";
        char Char[25];
        size_t Length = pathname.copy(Char, pathname.length() + 1);
        Char[Length] = '\0';
        tr.writeToFile(Char);
        /* create SymbolicSet-s representing the rabin pairs */
        scots::SymbolicSet spec(*stateSpace_);
        for (size_t i=0; i<RabinPairs_.size(); i++) {
            /* write the G sets */
            spec.setSymbolicSet(RabinPairs_[i].G_);
            std::string pathname = foldername;
            pathname += "/G";
            pathname += std::to_string(i);
            pathname += ".bdd";
            char Char[20];
            size_t Length = pathname.copy(Char, pathname.length() + 1);
            Char[Length] = '\0';
            spec.writeToFile(Char);
            /* write the nR sets */
            spec.setSymbolicSet(RabinPairs_[i].nR_);
            pathname = foldername;
            pathname += "/nR";
            pathname += std::to_string(i);
            pathname += ".bdd";
            Length = pathname.copy(Char, pathname.length() + 1);
            Char[Length] = '\0';
            spec.writeToFile(Char);
        }
    }
private:
    /* function: dec2bin
     * decimal to binary conversion
     *
     * Input:
     * n - a number in decimal
     * Output:
     * b - a vector representing the bits of the binary representation, where b[0] is LSB */
    std::vector<bool> dec2bin(const size_t n) {
        std::vector<bool> b;
        if (n==0) {
            b.push_back(0);
            return b;
        }
        size_t m=n;
        while (m>0) {
            div_t d=div(m,2);
            b.push_back(d.rem);
            m=d.quot;
        }
        return b;
    }
}; /* close class def */
} /* close namespace */

#endif /* RABINAUTOMATON_HH_ */
