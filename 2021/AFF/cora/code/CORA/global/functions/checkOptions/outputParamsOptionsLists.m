function [paramsList,optionsList] = outputParamsOptionsLists
%OUTPUTPARAMSOPTIONSLISTS Summary of this function goes here
%   Detailed explanation goes here

% read global variables
global fullParamsList;
global fullOptionsList;

% write global variables to output arguments
paramsList = fullParamsList;
optionsList = fullOptionsList;

% delete global variables
clear fullParamsList;
clear fullOptionsList;

end

