function res = inEllipsoid(E1,E2)
% inEllipsoid - checks whether ellipsoid E2 is contained in ellipsoid E1
%
% Syntax:  
%    E = inEllipsoid(E1,E2)
%
% Inputs:
%    E1,E2      - ellipsoid object
%
% Outputs:
%    res - true if contained, false otherwise
%
% References:
%            [1] Yildirim, E.A., 2006. On the minimum volume covering 
%                ellipsoid of ellipsoids. SIAM Journal on Optimization, 
%                17(3), pp.621-641.     
%            [2] SDPT3: url: http://www.math.nus.edu.sg/~mattohkc/sdpt3.html
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Gassmann
% Written:      16-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
if dim(E1) ~= dim(E1)
    error('dimensions do not match');
elseif E1.isdegenerate && E2.isdegenerate
    error('At least one ellipsoid has to be full-dimensional');
end

% if E1 is the degenerate one, E2 cannot be contained
if E1.isdegenerate
    res = false;
    return;
end

TOL = min(E1.TOL,E2.TOL);
%follows from the fact that Q2-Q1>=0 (psd) together with Q1,Q2 invertible
%implies inv(A)>=inv(B).
q_abs = max(abs([E1.q;E2.q]));
if q_abs == 0
    q_abs = 1;
end
if all(abs(E1.q-E2.q)/q_abs<=TOL) && min(eig(E2.Q-E1.Q))>=0
    res = true;
    return;
end

if E2.isdegenerate
    [T,~,~] = svd(E2.Q);
    nt = E2.rank;
    E2 = T'*E2;
    E1 = T'*E1;
    % project
    x2_rem = E2.q(nt+1:end);
    E2 = project(E2,1:nt);
    % resolve x2_rem in E1t by cutting with hyperplanes 
    % I(nt+1:end,:)*xt = x_rem
    n = E1.dim;
    I = eye(n);
    for i=1:(n-nt)
        Hi = conHyperplane(I(nt+i,:),x2_rem(i));
        E1 = E1 & Hi;
        % if intersection is empty, E2 cannot be contained in E1
        if isempty(E1)
            res = false;
            return;
        end
    end
    % now, E1 also has "0"s at nt+1:end
    E1 = project(E1,1:nt);
end

% normalize to prevent numerical issues
scale = 1;%mean([svd(E1.Q);svd(E2.Q)]);
E1 = 1/scale*E1;
E2 = 1/scale*E2;
Qs = E1.Q;
Q = E2.Q;
qs = E1.q;
q = E2.q;

%For more details, see [1]
t = sdpvar;
Qs_ = inv(Qs);
Q_ = inv(Q);
M = [Q_, -Q_*q;
      -(Q_*q)', q'*Q_*q-1];
Ms = [Qs_, -Qs_*qs;
      -(Qs_*qs)', qs'*Qs_*qs-1];

options = sdpsettings('verbose',0);
%when solving problems with higher-dimensional ellipsoids, sdpt3 [2] is
%recommended to avoid numerical issues
if exist('sdpt3','file')
    options.solver = 'sdpt3';
end
diagnostics = optimize(t*M>=Ms,[],options);
%either feasible or not feasible
if diagnostics.problem~=1 && diagnostics.problem~=0
    error('error with solving feasibility problem!');
end
if value(t)==0
    error('problem with solution of feasibility problem (t)');
end
res = ~diagnostics.problem;
%------------- END OF CODE --------------