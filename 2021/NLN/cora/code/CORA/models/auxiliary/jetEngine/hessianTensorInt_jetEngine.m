function Hf=hessianTensorInt_jetEngine(x,u)



 Hf{1} = interval(sparse(3,3),sparse(3,3));

Hf{1}(1,1) = - 3*x(1) - 3;


 Hf{2} = interval(sparse(3,3),sparse(3,3));

