function initParamsOptionsLists
%INITPARAMSOPTIONSLISTS Summary of this function goes here
%   Detailed explanation goes here

% define as global variables for simpler syntax in config file
global fullParamsList;
global fullOptionsList;

% set to empty struct
fullParamsList = struct();
fullOptionsList = struct();

% introduce fields for params list
fullParamsList.name = {};
fullParamsList.status = {};
fullParamsList.checkfuncs = {};
fullParamsList.errmsgs = {};
fullParamsList.condfunc = {};

% introduce fields for options list
fullOptionsList.name = {};
fullOptionsList.status = {};
fullOptionsList.checkfuncs = {};
fullOptionsList.errmsgs = {};
fullOptionsList.condfunc = {};

end

